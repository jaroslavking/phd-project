﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public class CombinationModels
    {
        public int one = 8;
        public int two = 20;
        public int three = 13;
        public int four = 35;
        public int five = 34;
        public int six = 3;
        public int seven =9;

        public int one_1 = 8;
        public int two_2 = 20;
        public int three_3 = 7;
        public int four_4 = 39;
        public int five_5 = 28;
        public int six_6 = 3;
        public int seven_7 = 9;


        public void ChangeCombinationType(int number)
        {
            switch (number)
            {
                case 1:
                    //1-2, 2-2
                    this.one = this.two;
                    this.one_1 = this.two_2;
                    break;
                case 2:
                    //3-2, 3-3, 3-4
                    this.two = this.three;
                    this.four = three;
                    this.two_2 = this.three_3;
                    this.four_4 = three_3;
                    break;
                case 3:
                    //4-4, 4-5
                    this.five = this.four;
                    this.five_5 = this.four_4;
                    break;
                case 4:
                    //4-3,4-4, 4-5
                    this.five = this.four;
                    this.three = this.four;
                    this.five_5 = this.four_4;
                    this.three_3 = this.four_4;
                    break;
                case 5:
                    //4-1,4-2, 4-3, 5-4, 5-5
                    this.one = this.four;
                    this.two = this.four;
                    this.three = this.four;
                    this.four = this.five;
                    this.one_1 = this.four_4;
                    this.two_2 = this.four_4;
                    this.three_3 = this.four_4;
                    this.four_4 = this.five_5;
                    break;
                case 6:
                    //2-1
                    this.two = this.one;
                    this.two_2 = this.one_1;
                    break;
                case 7:
                    //1-2 3-4
                    this.one = this.two;
                    this.three = four;
                    this.one_1 = this.two_2;
                    this.three_3 = four_4;
                    break;
                case 8:
                    //2-1 5-4
                    this.one = this.two;
                    this.four = five;
                    this.one_1 = this.two_2;
                    this.four_4 = five_5;
                    break;
                case 9:
                    //1-2, 3-2, 4-3, 5-4
                    this.two = this.one;
                    this.three = this.two;
                    this.four = this.three;
                    this.five = this.four;

                    this.two_2 = this.one_1;
                    this.three_3 = this.two_2;
                    this.four_4 = this.three_3;
                    this.five_5 = this.four_4;
                    break;
                case 10:
                    //1-2, 3-2, 4-3
                    this.two = this.one;
                    this.three = this.two;
                    this.four = this.three;

                    this.two_2 = this.one_1;
                    this.three_3 = this.two_2;
                    this.four_4 = this.three_3;

                    break;

                case 11:
                    //1-2, 2-2
                    this.one = this.three;
                    this.one_1 = this.three_3;
                    break;
            }
        }
    }
}

