﻿using System;
using System.Collections;
using System.Collections.Generic;
using NeuronDotNet.Core.Backpropagation;

using ConsoleApplication1.GA;

namespace ConsoleApplication1
{
    public class Prediction1
    {
        public static BackpropagationNetwork network;
        private double[] sunspots;
        //private double[] sunspots;
        public double[] predictions;
        public int nomerOstannohoTerazy;
        public int[] prohnozovanyjTeraz;
        //dlja zahalnoi hry!!!!!!!!!!!!!!!!
        public int[] holovnaHraPredictions;
        public int[] period;
        private List<int> predictFullColumn = new List<int>();
        public List<int> predictionNumbers = new List<int>();
        public int chastota;
        public static void setNetworkWeights(BackpropagationNetwork aNetwork, double[] weights)
        {
            // Setup the network's weights.
            int index = 0;

            foreach (BackpropagationConnector connector in aNetwork.Connectors)
            {
                foreach (BackpropagationSynapse synapse in connector.Synapses)
                {
                    synapse.Weight = weights[index++];
                    synapse.SourceNeuron.SetBias(weights[index++]);
                }
            }
        }

        public static double fitnessFunction(double[] weights)
        {
            double fitness = 0;

            setNetworkWeights(network, weights);

            // AND
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            /*// OR
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;*/

            /*// XOR
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;*/

            return fitness;
        }

        public int NeuralNetworkPredict(int[] arr)
        {
            LinearLayer inputLayer = new LinearLayer(2);
            SigmoidLayer hiddenLayer = new SigmoidLayer(2);
            SigmoidLayer outputLayer = new SigmoidLayer(1);

            BackpropagationConnector connector = new BackpropagationConnector(inputLayer, hiddenLayer);
            BackpropagationConnector connector2 = new BackpropagationConnector(hiddenLayer, outputLayer);
            network = new BackpropagationNetwork(inputLayer, outputLayer);
            network.Initialize();

            GA.GA ga = new GA.GA(0.50, 0.01, 100, 2000, 12);
            ga.FitnessFunction = new GAFunction(fitnessFunction);
            ga.Elitism = true;
            ga.Go();

            double[] weights;
            double fitness;
            ga.GetBest(out weights, out fitness);
            Console.WriteLine("Best brain had a fitness of " + fitness);

            setNetworkWeights(network, weights);




            double[] output = network.Run(new double[2] { 220.0, 100.0 });
            Console.WriteLine("Output: " + output[0]);


            return 0;

        }

        public int ARIMAModelPredict(int[] arr, int number, int kilkistProhnozovanyxNomeriv, bool holovnaHra)
        {
           
            predictions = new double[kilkistProhnozovanyxNomeriv];
            prohnozovanyjTeraz = new int[kilkistProhnozovanyxNomeriv];
           
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != number)
                {
                    break;
                }
                
            }
            
            int j = arr.Length;
            do
            {
                while (true)
                {
                    if (j == -1 ||j-1==-1)
                    {
                        break;
                    }
                    if (arr[j - 1] == number)
                    {
                        this.nomerOstannohoTerazy = j;
                        break;
                    }
                    j--;
                }
            } while (j < 0);

            //vyznachennja  kotryx terazax vypadav nomer
            for (int ij = 0; ij < arr.Length; ij++)
            {
                if (arr[ij] == number)
                {
                    this.chastota++;
                    predictFullColumn.Add(ij);

                }
            }
           
            int frekw = this.chastota + 2;
            int frekw1 = this.chastota + 1;
            int prognozNumber = (frekw * this.nomerOstannohoTerazy) / frekw1;

            if (//prognozNumber <= arr.Length + 300
                //|| 
                prognozNumber >= arr.Length && prognozNumber <= arr.Length + 30 ||
               // prognozNumber <= arr.Length && prognozNumber >= arr.Length -10
               prognozNumber <= arr.Length && prognozNumber >= arr.Length - 50
                )
            {
                predictionNumbers.Add(number);
            }
            
            return 1;
        }

        public int[] GetPeriodForNumForDB(int[] arr, int number)
        {
            int kilkist = 0;
            int[] terazNomery = new int[arr.Length];

            ArrayList period1 = new ArrayList();

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == number)
                {
                    kilkist++;
                    period1.Add(i);
                }
            }
            period = new int[period1.Count];
            for (int i = 0; i < period1.Count; i++)
            {
                if (i != 0)
                    period[i] = Convert.ToInt32(period1[i]) - Convert.ToInt32(period1[i - 1]);
            }

            return period;

        }
    }
}
