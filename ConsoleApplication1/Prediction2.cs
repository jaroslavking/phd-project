﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NeuronDotNet.Core.Backpropagation;
using NeuronDotNet.Core;
using Extreme.Mathematics;
using Extreme.Mathematics.LinearAlgebra;
using Extreme.Statistics;
using Extreme.Statistics.TimeSeriesAnalysis;
using ConsoleApplication1.GA;

namespace ConsoleApplication1
{
    public class Prediction2
    {
        public static BackpropagationNetwork network;
        private double[] sunspots;
        //private double[] sunspots;
        public double[] predictions;
        public int nomerOstannohoTerazy;
        public int[] prohnozovanyjTeraz;
        //dlja zahalnoi hry!!!!!!!!!!!!!!!!
        public int[] holovnaHraPredictions;
        public int[] period;
        private List<int> predictFullColumn = new List<int>();
        private List<int> predictionNumbers = new List<int>();
        public int chastota;
       

        public int ARIMAModelPredict(int[] arr, int number, int kilkistProhnozovanyxNomeriv, bool holovnaHra)
        {

            predictions = new double[kilkistProhnozovanyxNomeriv];
            prohnozovanyjTeraz = new int[kilkistProhnozovanyxNomeriv];
            //dlja zahalnoji hry!!!!!!!!!!!!!!!!!

            // This QuickStart Sample fits an ARMA(2,1) model and
            // an ARIMA(0,1,1) model to sunspot data.

            // The time series data is stored in a numerical variable:
         


            //Vybraty ostannij nomer terazy v jakomy vypadav nomer
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != number)
                {
                    break;
                }

            }
            //GetPeriodForNumForDB( arr, number);
            //sunspots = new double[period.Length];

            int j = arr.Length;
            do
            {
                while (true)
                {
                    if (j == -1 || j - 1 == -1)
                    {
                        break;
                    }
                    if (arr[j - 1] == number)
                    {
                        this.nomerOstannohoTerazy = j;
                        break;
                    }
                    j--;
                }
            } while (j < 0);

            //vyznachennja  kotryx terazax vypadav nomer
            for (int ij = 0; ij < arr.Length; ij++)
            {
                if (arr[ij] == number)
                {
                    this.chastota++;
                    predictFullColumn.Add(ij);

                }
            }
            /*
            chastota = number.Chastota + 2;
            chastota1 = number.Chastota + 1;
            teraz = number.Teraz;
            this.n_prog = (chastota * teraz) / chastota1;
            sunspots = new double[this.chastota];*/
            /*
             for (int i = 0; i < period.Length; i++)
             {

                 sunspots[i] = Convert.ToDouble(predictFullColumn[i]);
             }

             */

            //zapovnennja masyvy danymy dlja prohnozyvannja
            
            for (int i = 0; i < period.Length; i++)
            {
                sunspots[i] = Convert.ToDouble(period[i]);
            }
            
            // ARMA models (no differencing) are constructed from
            // the variable containing the time series data, and the
            // AR and MA orders. The following constructs an ARMA(2,1)
            // model:
            
            ArimaModel model = new ArimaModel(new NumericalVariable(sunspots), 2, 1);

            // The Compute methods fits the model.
            model.Compute();
           

            // The model's Parameters collection contains the fitted values.
            // For an ARIMA(p,d,q) model, the first p parameters are the 
            // auto-regressive parameters. The last q parametere are the
            // moving average parameters.
            
            Console.WriteLine("Variable              Value    Std.Error  t-stat  p-Value");
            foreach (Parameter parameter in model.Parameters)
                // Parameter objects have the following properties:
                Console.WriteLine("{0,-20}{1,10:F5}{2,10:F5}{3,8:F2} {4,7:F4}",
                    // Name, usually the name of the variable:
                    parameter.Name,
                    // Estimated value of the parameter:
                    parameter.Value,
                    // Standard error:
                    parameter.StandardError,
                    // The value of the t statistic for the hypothesis that the parameter
                    // is zero.
                    parameter.Statistic,
                    // Probability corresponding to the t statistic.
                    parameter.PValue);


            // The log-likelihood of the computed solution is also available:
            Console.WriteLine("Log-likelihood: {0:F4}", model.GetLogLikelihood());
            // as is the Akaike Information Criterion (AIC):
            Console.WriteLine("AIC: {0:F4}", model.GetAkaikeInformationCriterion());
            // and the Baysian Information Criterion (BIC):
            Console.WriteLine("BIC: {0:F4}", model.GetBayesianInformationCriterion());

            // The Forecast method can be used to predict the next value in the series...

            double nextValue = model.Forecast();
            Console.WriteLine("One step ahead forecast: {0:F3}", nextValue);

            // or to predict a specified number of values:

            //Prohnozyvannja!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            var nextValues = model.Forecast(kilkistProhnozovanyxNomeriv);

            Console.WriteLine("First " + kilkistProhnozovanyxNomeriv + " forecasts: {0:F3}", nextValues);
            /*
            int frekw = this.chastota + 2;
            int frekw1 = this.chastota + 1;
            int prognozNumber = (frekw * this.nomerOstannohoTerazy) / frekw1;

            if (//prognozNumber <= arr.Length + 300
                //|| 
                prognozNumber >= arr.Length && prognozNumber <= arr.Length + 20 ||
               // prognozNumber <= arr.Length && prognozNumber >= arr.Length -10
               prognozNumber <= arr.Length && prognozNumber >= arr.Length - 30
                )
            {
                predictionNumbers.Add(number);
            }
            */
            
            for (int i = 0; i < predictions.Length; i++)
            {
                predictions[i] = Convert.ToDouble(nextValues[i]);

                if (predictions[i] >= arr.Length && predictions[i] <= arr.Length + 20 ||
                    // prognozNumber <= arr.Length && prognozNumber >= arr.Length -10
                    predictions[i] <= arr.Length && predictions[i] >= arr.Length - 30
                  )
                              {
                                  predictionNumbers.Add(number);
                                  break;
                              }
                          }
                              /*
                              //Prohnozovanyj NomerTerazy
                              int frekw = this.chastota + 2;
                              int frekw1 = this.chastota + 1;
                              int prognozNumber = (frekw*this.nomerOstannohoTerazy)/frekw1;*/
                              /*
                              for (int i = 0; i < predictions.Length; i++)
                              {

                                  prohnozovanyjTeraz[i] = Convert.ToInt32(predictions[i]) + this.nomerOstannohoTerazy;

                                  if (prohnozovanyjTeraz[i] < arr.Length && prohnozovanyjTeraz[i]<= prognozNumber ||
                                      prohnozovanyjTeraz[i] < arr.Length && prohnozovanyjTeraz[i] >= prognozNumber||
                                      prohnozovanyjTeraz[i] > arr.Length+1|| prohnozovanyjTeraz[i] > arr.Length + 2||
                                      prohnozovanyjTeraz[i] > arr.Length + 3 || prohnozovanyjTeraz[i] > arr.Length + 4||
                                      prohnozovanyjTeraz[i] > arr.Length + 5 || prohnozovanyjTeraz[i] > arr.Length + 6||
                                      prohnozovanyjTeraz[i] > arr.Length + 7 || prohnozovanyjTeraz[i] > arr.Length + 8||
                                      prohnozovanyjTeraz[i] > arr.Length + 9 || prohnozovanyjTeraz[i] > arr.Length + 10
                                          && prohnozovanyjTeraz[i] <= prognozNumber ||
                                          prohnozovanyjTeraz[i] > arr.Length + 1 || prohnozovanyjTeraz[i] > arr.Length + 2 ||
                                      prohnozovanyjTeraz[i] > arr.Length + 3 || prohnozovanyjTeraz[i] > arr.Length + 4 ||
                                      prohnozovanyjTeraz[i] > arr.Length + 5 
                                          && prohnozovanyjTeraz[i] >= prognozNumber)
                                  {
                                       predictionNumbers.Add(number);
                                      break;
                                  }
                              }*/
                              // An integrated model (with differencing) is constructed
                              // by supplying the degree of differencing. Note the order
                              // of the orders is the traditional one for an ARIMA(p,d,q)
                              // model (p, d, q).
                              // The following constructs an ARIMA(0,1,1) model:
                              
                              ArimaModel model2 = new ArimaModel(new NumericalVariable(sunspots), 0, 1, 1);

                              // By default, the mean is assumed to be zero for an integrated model.
                              // We can override this by setting the EstimateMean property to true:
                              model2.EstimateMean = true;

                              // The Compute methods fits the model.
                              model2.Compute();

                              // The mean shows up as one of the parameters.
                              Console.WriteLine("Variable              Value    Std.Error  t-stat  p-Value");
                              foreach (Parameter parameter in model2.Parameters)
                                  Console.WriteLine("{0,-20}{1,10:F5}{2,10:F5}{3,8:F2} {4,7:F4}",
                                      parameter.Name,
                                      parameter.Value,
                                      parameter.StandardError,
                                      parameter.Statistic,
                                      parameter.PValue);

                              // We can also get the error variance:
                              Console.WriteLine("Error variance: {0:F4}", model2.ErrorVariance);

                              Console.WriteLine("Log-likelihood: {0:F4}", model2.GetLogLikelihood());
                              Console.WriteLine("AIC: {0:F4}", model2.GetAkaikeInformationCriterion());
                              Console.WriteLine("BIC: {0:F4}", model2.GetBayesianInformationCriterion());
                              Console.WriteLine("FOR NUMBER" + number);

                              //int frekw = this.chastota + 2;
                              //int frekw1 = this.chastota + 1;
                              //int prognozNumber = (frekw * this.nomerOstannohoTerazy) / frekw1;
                              //Console.Write("Press any key to exit.");
                              // Console.ReadLine();
            return 1;
        }

        public int[] GetPeriodForNumForDB(int[] arr, int number)
        {
            int kilkist = 0;
            int[] terazNomery = new int[arr.Length];

            ArrayList period1 = new ArrayList();

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == number)
                {
                    kilkist++;
                    period1.Add(i);
                }
            }
            period = new int[period1.Count];
            for (int i = 0; i < period1.Count; i++)
            {
                if (i != 0)
                    period[i] = Convert.ToInt32(period1[i]) - Convert.ToInt32(period1[i - 1]);
            }

            return period;

        }
    }
}

