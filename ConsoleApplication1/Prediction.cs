﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuronDotNet.Core.Backpropagation;
using NeuronDotNet.Core;
using Extreme.Mathematics;
using Extreme.Mathematics.LinearAlgebra;
using Extreme.Statistics;
using Extreme.Statistics.TimeSeriesAnalysis;
using ConsoleApplication1.GA;

namespace ConsoleApplication1
{
    public class Prediction
    {
        public static BackpropagationNetwork network;
        private double[] sunspots;
        public double[] predictions;
        public int nomerOstannohoTerazy;
        public int[] prohnozovanyjTeraz;
        //dlja zahalnoi hry!!!!!!!!!!!!!!!!
        public int[] holovnaHraPredictions;
        public static void setNetworkWeights(BackpropagationNetwork aNetwork, double[] weights)
        {
            // Setup the network's weights.
            int index = 0;

            foreach (BackpropagationConnector connector in aNetwork.Connectors)
            {
                foreach (BackpropagationSynapse synapse in connector.Synapses)
                {
                    synapse.Weight = weights[index++];
                    synapse.SourceNeuron.SetBias(weights[index++]);
                }
            }
        }

        public static double fitnessFunction(double[] weights)
        {
            double fitness = 0;

            setNetworkWeights(network, weights);

            // AND
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            /*// OR
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;*/

            /*// XOR
            double output = network.Run(new double[2] { 0, 0 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;

            output = network.Run(new double[2] { 0, 1 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 0 })[0];
            // The closest the output is to one, the more fit it is.
            fitness += output;

            output = network.Run(new double[2] { 1, 1 })[0];
            // The closest the output is to zero, the more fit it is.
            fitness += 1 - output;*/

            return fitness;
        }

        public int NeuralNetworkPredict(int[] arr)
        {
            LinearLayer inputLayer = new LinearLayer(2);
            SigmoidLayer hiddenLayer = new SigmoidLayer(2);
            SigmoidLayer outputLayer = new SigmoidLayer(1);

            BackpropagationConnector connector = new BackpropagationConnector(inputLayer, hiddenLayer);
            BackpropagationConnector connector2 = new BackpropagationConnector(hiddenLayer, outputLayer);
            network = new BackpropagationNetwork(inputLayer, outputLayer);
            network.Initialize();

            GA.GA ga = new GA.GA(0.50, 0.01, 100, 2000, 12);
            ga.FitnessFunction = new GAFunction(fitnessFunction);
            ga.Elitism = true;
            ga.Go();

            double[] weights;
            double fitness;
            ga.GetBest(out weights, out fitness);
            Console.WriteLine("Best brain had a fitness of " + fitness);

            setNetworkWeights(network, weights);

           
           
                
                        double[] output = network.Run(new double[2] { 220.0, 100.0});
                        Console.WriteLine("Output: " + output[0]);


            return 0;

        }

        public int ARIMAModelPredict(int[] arr, int number, int[] arr1, int kilkistProhnozovanyxNomeriv, bool holovnaHra)
        {
             sunspots= new double[arr.Length];
          
            //dlja zahalnoji hry!!!!!!!!!!!!!!!!!
            holovnaHraPredictions = new int[kilkistProhnozovanyxNomeriv];
            // This QuickStart Sample fits an ARMA(2,1) model and
            // an ARIMA(0,1,1) model to sunspot data.

            // The time series data is stored in a numerical variable:
            /* NumericalVariable sunspots = new NumericalVariable(Vector.Create(new double[] {
                 100.8, 81.6, 66.5, 34.8, 30.6, 7, 19.8, 92.5,
                 154.4, 125.9, 84.8, 68.1, 38.5, 22.8, 10.2, 24.1, 82.9,
                 132, 130.9, 118.1, 89.9, 66.6, 60, 46.9, 41, 21.3, 16,
                 6.4, 4.1, 6.8, 14.5, 34, 45, 43.1, 47.5, 42.2, 28.1, 10.1,
                 8.1, 2.5, 0, 1.4, 5, 12.2, 13.9, 35.4, 45.8, 41.1, 30.4,
                 23.9, 15.7, 6.6, 4, 1.8, 8.5, 16.6, 36.3, 49.7, 62.5, 67,
                 71, 47.8, 27.5, 8.5, 13.2, 56.9, 121.5, 138.3, 103.2,
                 85.8, 63.2, 36.8, 24.2, 10.7, 15, 40.1, 61.5, 98.5, 124.3,
                 95.9, 66.5, 64.5, 54.2, 39, 20.6, 6.7, 4.3, 22.8, 54.8,
                 93.8, 95.7, 77.2, 59.1, 44, 47, 30.5, 16.3, 7.3, 37.3,
                 73.9}));*/

            //zapovnennja masyvy danymy dlja prohnozyvannja
            for (int i = 0; i < arr.Length; i++)
            {
                sunspots[i] = Convert.ToDouble(arr[i]);
            }

            // ARMA models (no differencing) are constructed from
            // the variable containing the time series data, and the
            // AR and MA orders. The following constructs an ARMA(2,1)
            // model:
            ArimaModel model = new ArimaModel(new NumericalVariable(sunspots), 2, 1);

            // The Compute methods fits the model.
            model.Compute();

            // The model's Parameters collection contains the fitted values.
            // For an ARIMA(p,d,q) model, the first p parameters are the 
            // auto-regressive parameters. The last q parametere are the
            // moving average parameters.
            Console.WriteLine("Variable              Value    Std.Error  t-stat  p-Value");
            foreach (Parameter parameter in model.Parameters)
                // Parameter objects have the following properties:
                Console.WriteLine("{0,-20}{1,10:F5}{2,10:F5}{3,8:F2} {4,7:F4}",
                    // Name, usually the name of the variable:
                    parameter.Name,
                    // Estimated value of the parameter:
                    parameter.Value,
                    // Standard error:
                    parameter.StandardError,
                    // The value of the t statistic for the hypothesis that the parameter
                    // is zero.
                    parameter.Statistic,
                    // Probability corresponding to the t statistic.
                    parameter.PValue);


            // The log-likelihood of the computed solution is also available:
            Console.WriteLine("Log-likelihood: {0:F4}", model.GetLogLikelihood());
            // as is the Akaike Information Criterion (AIC):
            Console.WriteLine("AIC: {0:F4}", model.GetAkaikeInformationCriterion());
            // and the Baysian Information Criterion (BIC):
            Console.WriteLine("BIC: {0:F4}", model.GetBayesianInformationCriterion());

            // The Forecast method can be used to predict the next value in the series...
            
            double nextValue = model.Forecast();
            Console.WriteLine("One step ahead forecast: {0:F3}", nextValue);

            // or to predict a specified number of values:
          
            //Prohnozyvannja!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            var nextValues = model.Forecast(kilkistProhnozovanyxNomeriv);
          
            Console.WriteLine("First "+kilkistProhnozovanyxNomeriv+" forecasts: {0:F3}", nextValues);
            if (holovnaHra)
            {
                for (int i = 0; i < holovnaHraPredictions.Length; i++)
                {
                    holovnaHraPredictions[i] = Convert.ToInt32(nextValues[i]);
                }
            }

            
            // An integrated model (with differencing) is constructed
            // by supplying the degree of differencing. Note the order
            // of the orders is the traditional one for an ARIMA(p,d,q)
            // model (p, d, q).
            // The following constructs an ARIMA(0,1,1) model:
            ArimaModel model2 = new ArimaModel(new NumericalVariable(sunspots), 0, 1, 1);

            // By default, the mean is assumed to be zero for an integrated model.
            // We can override this by setting the EstimateMean property to true:
            model2.EstimateMean = true;

            // The Compute methods fits the model.
            model2.Compute();

            // The mean shows up as one of the parameters.
            Console.WriteLine("Variable              Value    Std.Error  t-stat  p-Value");
            foreach (Parameter parameter in model2.Parameters)
                Console.WriteLine("{0,-20}{1,10:F5}{2,10:F5}{3,8:F2} {4,7:F4}",
                    parameter.Name,
                    parameter.Value,
                    parameter.StandardError,
                    parameter.Statistic,
                    parameter.PValue);

            // We can also get the error variance:
            Console.WriteLine("Error variance: {0:F4}", model2.ErrorVariance);

            Console.WriteLine("Log-likelihood: {0:F4}", model2.GetLogLikelihood());
            Console.WriteLine("AIC: {0:F4}", model2.GetAkaikeInformationCriterion());
            Console.WriteLine("BIC: {0:F4}", model2.GetBayesianInformationCriterion());
            Console.WriteLine("FOR NUMBER"+ number);
            //Console.Write("Press any key to exit.");
           // Console.ReadLine();
            return 1;
        }
    }
}
